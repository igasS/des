create table user (
    id bigint not null auto_increment unique,
    username varchar(20) not null unique,
    password varchar(20) not null,
    company varchar(20) not null,
    role varchar(20) not null,
    created_on timestamp not null default now(),
    modified_on timestamp not null default now() on update now(),
    primary key(id)
);

create table template (
    id bigint not null auto_increment unique,
    name varchar(255) not null,
    description varchar(4000),
    file blob,
    created_on timestamp not null default now(),
    modified_on timestamp not null default now() on update now(),
    primary key(id)
);

create table exposition (
    id bigint not null auto_increment unique,
    name varchar(255) not null,
    description varchar(4000),
    url varchar(20) not null unique,
    created_on timestamp not null default now(),
    modified_on timestamp not null default now() on update now(),
    primary key(id)
);