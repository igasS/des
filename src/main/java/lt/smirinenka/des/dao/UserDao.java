package lt.smirinenka.des.dao;

import lt.smirinenka.des.entity.User;

public interface UserDao extends GenericDao<User> {

}
