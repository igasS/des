package lt.smirinenka.des.dao.impl;

import lt.smirinenka.des.dao.TemplateDao;
import lt.smirinenka.des.entity.Template;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TemplateDaoImpl extends GenericDaoImpl<Template> implements TemplateDao {

    @Autowired
    public TemplateDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Template.class);
    }
}
