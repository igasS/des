package lt.smirinenka.des.dao.impl;

import lt.smirinenka.des.dao.ExpositionDao;
import lt.smirinenka.des.entity.Exposition;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExpositionDaoImpl extends GenericDaoImpl<Exposition> implements ExpositionDao {

    @Autowired
    public ExpositionDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Exposition.class);
    }
}
