package lt.smirinenka.des.dao.impl;

import lt.smirinenka.des.dao.GenericDao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

abstract class GenericDaoImpl<T> implements GenericDao<T> {

    private SessionFactory sessionFactory;
    private Class<T> persistingClass;

    protected GenericDaoImpl(SessionFactory sessionFactory, Class<T> persistingClass) {
        this.sessionFactory = sessionFactory;
        this.persistingClass = persistingClass;
    }

    @Override
    public T get(long id) {
        return (T) getSession().get(persistingClass, id);
    }

    @Override
    public List<T> getAll() {
        Session session = getSession();
        List<T> list = (List<T>) session.createCriteria(persistingClass).list();
        return list;
    }

    @Override
    public void persist(T entity) {
        getSession().persist(entity);
    }

    @Override
    public void delete(T entity) {
        getSession().delete(entity);
    }

    @Override
    public void update(T entity) {
        getSession().update(entity);
    }

    protected Session getSession() throws HibernateException{
        Session session = sessionFactory.getCurrentSession();
        return session;
    }
}
