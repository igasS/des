package lt.smirinenka.des.dao;

import java.util.List;

public interface GenericDao<T> {

    T get(long id);

    List<T> getAll();

    void persist(T entity);

    void delete(T entity);

    void update(T entity);
}
