package lt.smirinenka.des.dao;

import lt.smirinenka.des.entity.Exposition;

public interface ExpositionDao extends GenericDao<Exposition> {
}
