package lt.smirinenka.des.dao;

import lt.smirinenka.des.entity.Template;

public interface TemplateDao extends GenericDao<Template> {
}
