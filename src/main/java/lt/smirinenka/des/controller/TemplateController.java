package lt.smirinenka.des.controller;

import lt.smirinenka.des.entity.Template;
import lt.smirinenka.des.form.TemplateForm;
import lt.smirinenka.des.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TemplateController {

    private final TemplateService templateService;

    @Autowired
    public TemplateController(TemplateService templateService) {
        this.templateService = templateService;
    }

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public ModelAndView templates(Model model) {
        model.addAttribute("templates", templateService.getAll());
        return new ModelAndView("template/templates");
    }

    @RequestMapping(value = "/template/new", method = RequestMethod.GET)
    public ModelAndView newTemplate() {
        return new ModelAndView("template/new").addObject(new TemplateForm());
    }

    @RequestMapping(value = "/template/new", method = RequestMethod.POST)
    public ModelAndView saveTemplate(TemplateForm templateForm, Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("template/new").addObject(errors).addObject(templateForm);
        } else {
            templateService.save(templateForm.parseEntity());
            return new ModelAndView("redirect:/app/template");
        }
    }

    @RequestMapping(value = "/template/{id}/update", method = RequestMethod.GET)
    public ModelAndView updateView(@PathVariable(value = "id") int id, TemplateForm templateForm) {
        Template template = templateService.get(id);
        templateForm.setId(template.getId());
        templateForm.setName(template.getName());
        templateForm.setDescription(template.getDescription());
        return new ModelAndView("template/edit").addObject(templateForm);
    }

    @RequestMapping(value = "/template/{id}/update", method = RequestMethod.POST)
    public ModelAndView update(@PathVariable(value = "id") int id, TemplateForm templateForm, Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("template/edit").addObject(errors).addObject(templateForm);
        } else {
            templateService.update(templateForm.parseEntity());
            return new ModelAndView("redirect:/app/template");
        }
    }

    @RequestMapping(value = "/template/{id}/delete", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable(value = "id") int id) {
        Template template = new Template();
        template.setId(id);
        templateService.delete(template);
        return new ModelAndView("redirect:/app/template");
    }

}
