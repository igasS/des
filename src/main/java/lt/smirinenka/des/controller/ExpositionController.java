package lt.smirinenka.des.controller;

import lt.smirinenka.des.entity.Exposition;
import lt.smirinenka.des.form.ExpositionForm;
import lt.smirinenka.des.service.ExpositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ExpositionController {

    private final ExpositionService expositionService;

    @Autowired
    public ExpositionController(ExpositionService expositionService) {
        this.expositionService = expositionService;
    }

    @RequestMapping(value = "/expo", method = RequestMethod.GET)
    public ModelAndView expositions(Model model) {
        model.addAttribute("expositions", expositionService.getAll());
        return new ModelAndView("exposition/expositions");
    }

    @RequestMapping(value = "/expo/new", method = RequestMethod.GET)
    public ModelAndView newExposition() {
        return new ModelAndView("exposition/new").addObject(new ExpositionForm());
    }

    @RequestMapping(value = "/expo/new", method = RequestMethod.POST)
    public ModelAndView save(ExpositionForm expositionForm, Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("exposition/new").addObject(errors).addObject(expositionForm);
        } else {
            expositionService.save(expositionForm.parseEntity());
            return new ModelAndView("redirect:/app/expo");
        }
    }

    @RequestMapping(value = "/expo/{id}/update", method = RequestMethod.GET)
    public ModelAndView updateView(@PathVariable(value = "id") int id, ExpositionForm expositionForm) {
        Exposition exposition = expositionService.get(id);
        expositionForm.setId(exposition.getId());
        expositionForm.setName(exposition.getName());
        expositionForm.setDescription(exposition.getDescription());
        expositionForm.setUrl(exposition.getUrl());
        return new ModelAndView("exposition/edit").addObject(expositionForm);
    }

    @RequestMapping(value = "/expo/{id}/update", method = RequestMethod.POST)
    public ModelAndView update(@PathVariable(value = "id") int id, ExpositionForm expositionForm, Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("exposition/{id}/edit").addObject(expositionForm).addObject(errors);
        } else {
            expositionService.update(expositionForm.parseEntity());
            return new ModelAndView("redirect:/app/expo");
        }
    }

    @RequestMapping(value = "/expo/{id}/delete", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable(value = "id") long id) {
        Exposition exposition = new Exposition();
        exposition.setId(id);
        expositionService.delete(exposition);
        return new ModelAndView("redirect:/app/expo");
    }
}