package lt.smirinenka.des.controller;

import lt.smirinenka.des.entity.User;
import lt.smirinenka.des.form.UserForm;
import lt.smirinenka.des.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ModelAndView users(Model model) {
        model.addAttribute("users", userService.getAll());
        return new ModelAndView("user/users");
    }

    @RequestMapping(value = "/user/new", method = RequestMethod.GET)
    public ModelAndView newUser(UserForm userForm) {
        return new ModelAndView("user/new").addObject(userForm);
    }

    @RequestMapping(value = "/user/new", method = RequestMethod.POST)
    public ModelAndView saveUser(UserForm userForm, Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("user/new").addObject(errors).addObject(userForm);
        } else {
            userService.save(userForm.parseEntity());
            return new ModelAndView("redirect:/app/user");
        }
    }

    @RequestMapping(value = "/user/{id}/update", method = RequestMethod.GET)
    public ModelAndView updateView(@PathVariable(value = "id") int id, UserForm userForm) {
        User user = userService.get(id);
        userForm.setId(user.getId());
        userForm.setUsername(user.getUsername());
        userForm.setPassword(user.getPassword());
        userForm.setCompany(user.getCompany());
        userForm.setRole(user.getRole());
        return new ModelAndView("user/edit").addObject(userForm);
    }

    @RequestMapping(value = "/user/{id}/update", method = RequestMethod.POST)
    public ModelAndView update(@PathVariable(value = "id") int id, UserForm userForm, Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("user/edit").addObject(errors).addObject(userForm);
        } else {
            userService.update(userForm.parseEntity());
            return new ModelAndView("redirect:/app/user");
        }
    }

    @RequestMapping(value = "/user/{id}/delete", method = RequestMethod.POST)
    public ModelAndView delete(@PathVariable(value = "id") int id) {
        User user = new User();
        user.setId(id);
        userService.delete(user);
        return new ModelAndView("redirect:/app/user");
    }
}