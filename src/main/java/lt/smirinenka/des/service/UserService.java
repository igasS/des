package lt.smirinenka.des.service;

import lt.smirinenka.des.entity.User;

import java.util.List;

public interface UserService {

    void save(User user);

    List<User> getAll();

    User get(int id);

    void update(User user);

    void delete(User user);
}
