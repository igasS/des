package lt.smirinenka.des.service;

import lt.smirinenka.des.entity.Exposition;

import java.util.List;

public interface ExpositionService {

    void save(Exposition exposition);

    List<Exposition> getAll();

    Exposition get(int id);

    void update(Exposition exposition);

    void delete(Exposition exposition);
}
