package lt.smirinenka.des.service.impl;

import lt.smirinenka.des.dao.ExpositionDao;
import lt.smirinenka.des.entity.Exposition;
import lt.smirinenka.des.service.ExpositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class ExpositionServiceImpl implements ExpositionService {

    private ExpositionDao expositionDao;

    @Autowired
    public ExpositionServiceImpl(ExpositionDao expositionDao) {
        this.expositionDao = expositionDao;
    }

    @Override
    @Transactional
    public void save(Exposition exposition) {
        expositionDao.persist(exposition);
    }

    @Override
    @Transactional
    public List<Exposition> getAll() {
        return expositionDao.getAll();
    }

    @Override
    @Transactional
    public Exposition get(int id) {
        return expositionDao.get(id);
    }

    @Override
    @Transactional
    public void update(Exposition exposition) {
        expositionDao.update(exposition);
    }

    @Override
    @Transactional
    public void delete(Exposition exposition) {
        expositionDao.delete(exposition);
    }
}