package lt.smirinenka.des.service.impl;

import lt.smirinenka.des.dao.TemplateDao;
import lt.smirinenka.des.entity.Template;
import lt.smirinenka.des.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class TemplateServiceImpl implements TemplateService {

    private TemplateDao templateDao;

    @Autowired
    public TemplateServiceImpl(TemplateDao templateDao) {
        this.templateDao = templateDao;
    }

    @Override
    @Transactional
    public void save(Template template) {
        templateDao.persist(template);
    }

    @Override
    @Transactional
    public List<Template> getAll() {
        return templateDao.getAll();
    }

    @Override
    @Transactional
    public Template get(int id) {
        return templateDao.get(id);
    }

    @Override
    @Transactional
    public void update(Template template) {
        templateDao.update(template);
    }

    @Override
    @Transactional
    public void delete(Template template) {
        templateDao.delete(template);
    }
}
