package lt.smirinenka.des.service;

import lt.smirinenka.des.entity.Template;

import java.util.List;

public interface TemplateService {

    void save(Template template);

    List<Template> getAll();

    Template get(int id);

    void update(Template template);

    void delete(Template template);
}
