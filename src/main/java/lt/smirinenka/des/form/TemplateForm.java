package lt.smirinenka.des.form;

import lt.smirinenka.des.entity.Template;

public class TemplateForm {

    private long id;
    private String name;
    private String description;

    public Template parseEntity() {
        Template template = new Template();
        template.setId(this.id);
        template.setName(this.name);
        template.setDescription(this.description);
        return template;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
