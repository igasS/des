package lt.smirinenka.des.form;

import lt.smirinenka.des.entity.Exposition;

public class ExpositionForm {

    private long id;
    private String name;
    private String description;
    private String url;

    public Exposition parseEntity() {
        Exposition exposition = new Exposition();
        exposition.setId(this.id);
        exposition.setName(this.name);
        exposition.setDescription(this.description);
        exposition.setUrl(this.url);
        return exposition;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
