<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head></head>
    <body>
        <form:form modelAttribute="templateForm" method="post">
            <input type="hidden" name="id" value=${templateForm.id} />
            <label for="title">Pavadinimas</label>
            <input type="text" name="name" id="title" value=${templateForm.name} />
            <br />
            <label for="description">Aprašas</label>
            <input type="text" name="description" id="description" value=${templateForm.description} />
            <br />
            <input type="submit" />
        </form:form>
    </body>
</html>