<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <span>Maketų sąrašas</span>
    <table>
        <tr>
            <th>Vieta ekspozicijoje</th>
            <th>Dizaino maketo miniatiūra</th>
            <th>Dizaino maketo aprašas</th>
            <th>Dizaino maketo įkėlimo laikas</th>
            <th></th>
        </tr>
        <c:forEach var="template" items="${templates}">
            <tr>
                <td></td>
                <td>${template.template}</td>
                <td>${template.name}</td>
                <td>${template.description}</td>
                <td>template.createdOn</td>
                <td>
                    <button><a href="">Aukštyn</a></button>
                    <button><a href="">Žemyn</a></button>
                    <button><a href="/des/app/template/${template.id}/update">Redaguoti</a></button>
                    <form id="deleteForm" action="/des/app/template/${template.id}/delete" method="post">
                        <input type="submit" value="Pašalinti" />
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>

    <a href="/des/app/template/new">Įkelti naują maketą</a>
</html>