<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head></head>
    <body>
        <form:form modelAttribute="templateForm" method="post">
            <label for="title">Pavadinimas</label>
            <input type="text" name="name" id="title" />
            <br />
            <label for="description">Aprašas</label>
            <input type="text" name="description" id="description" />
            <br />
            <input type="submit" />
        </form:form>
    </body>
</html>