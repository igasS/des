<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head></head>
    <body>
        <form:form modelAttribute="expositionForm" method="post">
            <label for="title">Pavadinimas</label>
            <input type="text" name="name" id="title" />
            <br />
            <label for="description">Aprašas</label>
            <input type="text" name="description" id="description" />
            <br />
            <label for="url">Url žymė</label>
            <input type="text" name="url" id="url" />
            <input type="submit" />
        </form:form>
    </body>
</html>