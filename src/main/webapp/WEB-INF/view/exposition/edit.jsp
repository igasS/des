<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head></head>
    <body>
        <form:form modelAttribute="expositionForm" method="post">
            <input type="hidden" name="id" value=${expositionForm.id} />
            <label for="title">Pavadinimas</label>
            <input type="text" name="name" id="title" value=${expositionForm.name} />
            <br />
            <label for="description">Aprašas</label>
            <input type="text" name="description" id="description" value=${expositionForm.description} />
            <br />
            <label for="url">Url žymė</label>
            <input type="text" name="url" id="url" value=${expositionForm.url} />
            <input type="submit" />
        </form:form>
    </body>
</html>