<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <span>Ekspozicijų sąrašas</span>
    <table>
        <tr>
            <th>Pavadinimas</th>
            <th>Aprašymas</th>
            <th>Url žymė</th>
        </tr>
        <c:forEach var="exposition" items="${expositions}">
            <tr>
                <td>${exposition.name}</td>
                <td>${exposition.description}</td>
                <td>${exposition.url}</td>
                <td>
                    <button><a href="">Peržiūrėti</a></button>
                    <button><a href="/des/app/expo/${exposition.id}/update">Redaguoti</a></button>
                    <form id="deleteForm" action="/des/app/expo/${exposition.id}/delete" method="post">
                        <input type="submit" value="Pašalinti" />
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a href="/des/app/expo/new">Sukurti naują ekspoziciją</a>
</html>