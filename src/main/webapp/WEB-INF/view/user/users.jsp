<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <span>Vartotojų sąrašas</span>
    <table>
        <tr>
            <th>Vartotojo vardas</th>
            <th>Įmonė</th>
            <th>Rolė</th>
            <th>Susietų ekspozicijų kiekis</th>
            <th></th>
        </tr>
        <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.username}</td>
                <td>${user.company}</td>
                <td>${user.role}</td>
                <td>user.expositions.size()</td>
                <td>
                    <button><a href="/des/app/user/${user.id}/update">Redaguoti</a></button>
                    <form id="deleteForm" action="/des/app/user/${user.id}/delete" method="post">
                        <input type="submit" value="Pašalinti" />
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>

    <a href="/des/app/user/new">Sukurti naują paskyrą</a>
</html>