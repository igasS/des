<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head></head>
    <body>
        <form:form modelAttribute="userForm" method="post">
            <input type="hidden" name="id" value=${userForm.id} />
            <label for="user">Vartotojo vardas</label>
            <input type="text" name="username" value=${userForm.username} id="user" />
            <br />
            <label for="pass">Slaptažodis</label>
            <input type="password" name="password" value=${userForm.password} id="pass" />
            <br />
            <label for="companyName">Kompanija</label>
            <input type="text" name="company" value=${userForm.company} id="companyName" />
            <br />
            <label for="roleName">Rolė</label>
            <input type="text" name="role" value=${userForm.role} id="roleName" />
            <br />
            <input type="submit" />
        </form:form>
    </body>
</html>