<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head></head>
    <body>
        <form:form modelAttribute="userForm" method="post">
            <label for="user">Vartotojo vardas</label>
            <input type="text" name="username" id="user" />
            <br />
            <label for="pass">Slaptažodis</label>
            <input type="password" name="password" id="pass" />
            <br />
            <label for="companyName">Kompanija</label>
            <input type="text" name="company" id="companyName" />
            <br />
            <label for="roleName">Rolė</label>
            <input type="text" name="role" id="roleName" />
            <br />
            <input type="submit" />
        </form:form>
    </body>
</html>